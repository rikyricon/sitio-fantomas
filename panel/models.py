from django.db.models import *
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from ckeditor.fields import RichTextField
# Create your models here.

class Manager(BaseUserManager):
    
    def _create_user(self, alias, correo, password, is_staff, is_superuser, **extra_fields):
        user = self.model(
            alias=alias,
            correo=correo,
            is_staff=is_staff,
            is_superuser=is_superuser,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self.db)
        return user

    def create_user(self, alias, correo, is_staff, password=None, **extra_fields):
        return self._create_user(alias, correo, password, is_staff, False, **extra_fields)

    def create_superuser(self, alias, correo, password=None, **extra_fields):
        return self._create_user(alias, correo, password, True, True, **extra_fields)

class Gerente(AbstractBaseUser, PermissionsMixin):
    alias = CharField(unique=True, max_length=100)
    correo = EmailField(max_length=254, unique=True)
    imagen = URLField('Imagen de perfil', blank=True, null=True)

    descripcion = TextField('Descripción', max_length=200)
    face = URLField('Perfil Facebook', blank=True, null=True)
    insta = URLField('Perfil Instagram', blank=True, null=True)
    yutu = URLField('Perfil Youtube', blank=True, null=True)
    fecha = DateField('Fecha de creacíon', auto_now=True, auto_now_add=False)

    # rol = ForeignKey(Rol, on_delete=models.CASCADE,blank = True,null = True)
    is_active = BooleanField('activo/inactivo', default=True)
    is_staff = BooleanField('Ingreso autorizado', default=False)
    objects = Manager()

    USERNAME_FIELD = 'correo'
    REQUIRED_FIELDS = ['alias']

    # class Meta:
    #     permissions = [('permiso_desde_codigo', 'Este es un permiso creado desde código'),
    #                    ('segundo_permiso_codigo', 'Segundo permiso creado desde codigo')]
    class Meta:
        db_table = "gerentes"
        verbose_name = "gerente"
        verbose_name_plural = "gerentes"

    def __str__(self):
        return f'{self.alias}'

class Categoria(Model):
    nombre = CharField(max_length=100)
    fecha = DateTimeField('Fecha de creacíon', auto_now=False, auto_now_add=True)

    class Meta:
        db_table = "categorias"
        verbose_name = "categoria"
        verbose_name_plural = "categorias"

    def __str__(self):
        return f'{self.nombre}'

class Articulo(Model):
    titulo = CharField('Título', max_length=100, blank=False, null=False)
    url = SlugField(max_length=90, blank=False, null=False)
    imagen = URLField('Imagen de perfil', blank=True, null=True)
    descripcion = RichTextField('Descripción', blank=False, null=False)
    autor = ForeignKey(Gerente, on_delete=CASCADE)
    categoria = ManyToManyField(Categoria)
    estado = BooleanField('Publicado/No Publicado', default=True)
    fecha = DateTimeField('Fecha de creacíon', auto_now=False, auto_now_add=True)

    class Meta:
        db_table = "articulos"
        verbose_name = "articulo"
        verbose_name_plural = "articulos"

    def __str__(self):
        return f'{self.titulo}'

class Config(Model):
    titulo = CharField(max_length=100)
    slogan = CharField(max_length=200)
    img = URLField('Imagen del sitio', blank=True, null=True)
    icono = URLField('Icono del sitio', blank=True, null=True)
    fecha = DateField('Fecha de creacíon', auto_now=False, auto_now_add=True)

    class Meta:
        db_table = "configuraciones"
        verbose_name = "configuración"
        verbose_name_plural = "configuraciónes"

    def __str__(self):
        return f'{self.titulo}'
