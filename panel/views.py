from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponseRedirect
from django.views.generic.base import TemplateView
from django.contrib.auth.views import LoginView
from django.views.generic.edit import FormView
from django.urls.base import reverse_lazy
from django.contrib.auth import login,logout
from django.shortcuts import redirect
from .forms import GerForm
# Create your views here.

class Panel(LoginRequiredMixin,TemplateView):
    login_url = '/login/'
    template_name = "panel/index.html"

class GerLogin(FormView):
    template_name = "panel/login.html"
    form_class = GerForm
    success_url = reverse_lazy('panel')

    def form_valid(self, form):
            login(self.request,form.user_cache)
            return super().form_valid(form)
    
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(GerLogin, self).dispatch(request, *args, **kwargs)

def logout_ger(request):
    logout(request)
    return redirect('login')
