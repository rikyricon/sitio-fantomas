from django.contrib.auth.forms import AuthenticationForm
from django.db.models.fields import EmailField
from django import forms

# class GerForm(ModelForm):
    
#     class Meta:
#         model = Gerente
#         fields = '__all__'
#         labels = {
#             'username': 'Correo :',          
#             'password': 'Contraseña :',          
#         }

#         widgets = {
#             'username': TextInput(
#                 attrs = {
#                     'class': 'border-0 border-b-2 border-transparent border-gray-200 bg-rose-900 w-full p-1 text-xl',
#                     'placeholder': 'Escribe tu correo',
#                     'id': 'username'
#                 }
#             ),

#             'password': TextInput(
#                 attrs = {
#                     'class': 'text-white border-0 border-b-2 border-transparent border-gray-200 w-full bg-rose-900 p-2',
#                     'placeholder': 'Escribe tu contraseña',
#                     'id': 'password',
#                     'type': 'password'
#                 }
#             )
#         }



class GerForm(AuthenticationForm):
    username = EmailField()

    def __init__(self,*args, **kwargs):
        super(GerForm, self).__init__(self,*args, **kwargs)
        self.fields['username'].widget = forms.EmailInput(attrs={'autofocus': True})
        self.fields['username'].widget.attrs.update({'class': 'text-white border-0 border-b-2 border-transparent border-gray-200 w-full bg-indigo-900 p-2'})
        self.fields['username'].widget.attrs.update({'id': 'name'})
        self.fields['username'].widget.attrs.update({'placeholder': 'Escribe tu correo'})

        self.fields['password'].widget.attrs.update({'class': 'text-white border-0 border-b-2 border-transparent border-gray-200 w-full bg-indigo-900 p-2'})
        self.fields['password'].widget.attrs.update({'id': 'pass'})
        self.fields['password'].widget.attrs.update({'placeholder': 'Escribe tu contraseña'})