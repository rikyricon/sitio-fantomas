from django.contrib.admin import ModelAdmin, site
from .models import Gerente, Articulo, Categoria, Config

# Register your models here.

class GrAdmin(ModelAdmin):
    list_display = ('alias', 'correo', 'imagen', 'is_superuser', 'fecha', 'is_active', 'is_staff',  )

class CatAdmin(ModelAdmin):
    list_display = ('nombre', 'fecha', )

class ArtAdmin(ModelAdmin):
    list_display = ('titulo', 'url', 'imagen', 'descripcion', 'autor', 'estado', 'fecha',)

class ConfAdmin(ModelAdmin):
    list_display = ('titulo', 'slogan', 'icono', 'img', 'fecha',)

site.register(Gerente, GrAdmin)
site.register(Categoria, CatAdmin)
site.register(Articulo, ArtAdmin)
site.register(Config, ConfAdmin)
