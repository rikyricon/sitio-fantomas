from panel.models import Config

def info_sitio(context):
    obj = None
    obj_list = Config.objects.all()
    if not obj_list:
        obj = type('test', (object,), {})()
        obj.titulo = ''
        obj.slogan = ''
        obj.img = ''
        obj.icono = ''
    else:
        obj = obj_list[0]

    return {
        'SITIO_TITULO': obj.titulo,
        'SITIO_SLOGAN': obj.slogan,
        'SITIO_IMG': obj.img,
        'SITIO_ICONO': obj.icono,
    }
